package controller;

import java.util.concurrent.TimeUnit;

public class UiControls {
//adding and deleting elements from ui

	public long countTimeElapsed(long startTime) {
		long endTime = System.nanoTime();
		long timeElapsed = endTime - startTime;

		return TimeUnit.SECONDS.convert(timeElapsed, TimeUnit.NANOSECONDS);
	}

}
