package model;

public class Drinks {
	private String drink;
	private int amount;
	
	public String getDrink() {
		return drink;
	}
	
	public void setDrink(String drink) {
		this.drink = drink;
	}
	
	public int getAmount() {
		return amount;
	}
	
	public void setAmount(int amount) {
		this.amount = amount;
	}
}
