package model;

public class Game {
	private int id;
	private String title;
	private int playCount;
	private String playTime;
	private String lastWinner;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getPlayCount() {
		return playCount;
	}

	public void setPlayCount(int playCount) {
		this.playCount = playCount;
	}

	public String getPlayTime() {
		return playTime;
	}

	public void setPlayTime(String playTime) {
		this.playTime = playTime;
	}

	public String getLastWinner() {
		return lastWinner;
	}

	public void setLastWinner(String lastWinner) {
		this.lastWinner = lastWinner;
	}

}
