package view;

import static utils.Dictionary.*;

import java.awt.CardLayout;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class PanelsAsCards extends JPanel {

	private static final long serialVersionUID = -6275842433549978556L;
	MenuPanel menuPanel;
	AddPanel editPanel;
	PlayPanel playPanel;

	JScrollPane menuCard, additionCard, playCard;

	public PanelsAsCards() {
		menuPanel = new MenuPanel(this);
		editPanel = new AddPanel(this);
		playPanel = new PlayPanel(this, null);

		menuCard = new JScrollPane(menuPanel);
		additionCard = new JScrollPane(editPanel);
		playCard = new JScrollPane(playPanel);

		setLayout(new CardLayout());

		add(menuCard, MENU);
		add(additionCard, ADDITION);
		add(playCard, PLAYING);
	}

	public void changePanels(String panelToShow) {
		CardLayout cl = (CardLayout) getLayout();
		cl.show(this, panelToShow);
	}

}
