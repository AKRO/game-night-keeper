package view;

import static utils.Dictionary.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.HashMap;

import javax.swing.*;

import database.SqliteOperations;
import database.SqliteSetup;
import model.Game;
import utils.Labels;

public class MenuPanel extends JPanel implements ActionListener {

	private static final long serialVersionUID = -7586244907640270734L;
	PanelsAsCards panelsAsCards;
	JPanel buttonPanel;
	JComboBox gameList;
	JLabel labelTimesPlayed = new JLabel("");
	JLabel labelTimeSpent = new JLabel("");
	JLabel labelLastWinner = new JLabel("");
	Labels labels = new Labels();
	HashMap<String, JLabel> labelsMap = new HashMap<String, JLabel>();
	Game game = new Game();

	private JButton buttonPlay = new JButton(PLAY_PANEL_BUTTON);
	private JButton buttonDrinks = new JButton(DRINKS_PANEL_BUTTON);
	private JButton buttonAdd = new JButton(ADD_PANEL_BUTTON);

	public MenuPanel(PanelsAsCards panelsAsCards) {
		this.panelsAsCards = panelsAsCards;
		createMenuPanel();
	}

	public void createMenuPanel() {
		setDesign();
		buttonPlay.addActionListener(this);
		buttonDrinks.addActionListener(this);
		buttonAdd.addActionListener(this);

	}

	private void setDesign() {
		setLayout(new BorderLayout());

		setButtonPanel();
		add(buttonPanel, BorderLayout.CENTER);

	}

	private void setButtonPanel() {
		buttonPanel = new JPanel();
		buttonPanel.setSize(127, 72);
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.Y_AXIS));

		SqliteSetup sqliteSetup = new SqliteSetup();
		sqliteSetup.openConnection();
		final SqliteOperations sqliteOperations = new SqliteOperations();
		sqliteOperations.setStatement(sqliteSetup.getStatement());

		gameList = new JComboBox(sqliteOperations.getAllGameTitlesFromDb());
		gameList.setAlignmentX(Component.LEFT_ALIGNMENT);

		buttonPlay.setAlignmentX(Component.CENTER_ALIGNMENT);
		buttonDrinks.setAlignmentX(Component.CENTER_ALIGNMENT);
		buttonAdd.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		
		ItemListener itemListener = new ItemListener() {
		      public void itemStateChanged(ItemEvent itemEvent) {
		        int state = itemEvent.getStateChange();
		        Object comboboxSelectedValue = itemEvent.getItem();
		        game = sqliteOperations.returnGameDataFromDb(comboboxSelectedValue.toString());
		        //jlabel.setText(comboboxSelectedValue.toString());
		        labelsMap = labels.prepareLabels(game);
		        labelTimesPlayed.setText(labelsMap.get("timesPlayed").getText());
		        labelTimeSpent.setText(labelsMap.get("timeSpent").getText());
		        labelLastWinner.setText(labelsMap.get("lastWinner").getText());
		      }
		    };
		
	        
		gameList.addItemListener(itemListener);
        buttonPanel.add(labelTimesPlayed);
        buttonPanel.add(labelTimeSpent);
        buttonPanel.add(labelLastWinner);
		buttonPanel.add(gameList);
		buttonPanel.add(Box.createRigidArea(new Dimension(25, 0)));
		buttonPanel.add(buttonPlay);
		buttonPanel.add(Box.createRigidArea(new Dimension(0, 25)));
		buttonPanel.add(buttonDrinks);
		buttonPanel.add(Box.createRigidArea(new Dimension(0, 25)));
		buttonPanel.add(buttonAdd);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == buttonPlay) {
			PlayPanel playPanel = new PlayPanel(panelsAsCards, gameList.getSelectedItem().toString());
			removeAll();
			add(playPanel, BorderLayout.CENTER);
			revalidate();
			panelsAsCards.changePanels(PLAYING);
		}
		if (e.getSource() == buttonDrinks) {
			DrinksPanel drinksPanel = new DrinksPanel(panelsAsCards);
			removeAll();
			add(drinksPanel, BorderLayout.CENTER);
			revalidate();
			panelsAsCards.changePanels(DRINKS);
		}
		if (e.getSource() == buttonAdd) {
			AddPanel addPanel = new AddPanel(panelsAsCards);
			removeAll();
			add(addPanel, BorderLayout.CENTER);
			revalidate();
			panelsAsCards.changePanels(ADDITION);
		}
	}
}
