package view;

import static utils.Dictionary.*;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

import javax.swing.*;

import controller.UiControls;
import database.*;
import model.Game;
import utils.Labels;

public class PlayPanel extends JPanel implements ActionListener {
	// logo?
	// text title
	// text times played
	// text time played
	// text last winner
	// dropbox winner
	// przycisk koniec
	private static final long serialVersionUID = 3828390521762030074L;
	String[] cardListOfOneCard = new String[1];

	PanelsAsCards panelsAsCards;
	private JPanel gamePanel = new JPanel();
	private JComboBox winnerBox;
	private JButton roundFinishedButton = new JButton(END_GAME_BUTTON);
	private JButton homeButton = new JButton(HOME_BUTTON);
	UiControls uiControls = new UiControls();
	SqliteSetup sqliteSetup = new SqliteSetup();
	SqliteOperations sqliteOperations = new SqliteOperations();
	Game game = new Game();
	private String gameTitle;
	private long startTime;
	HashMap<String, JLabel> labelsMap = new HashMap<String, JLabel>();
	Labels labels = new Labels();

	public PlayPanel(PanelsAsCards panelsAsCards, String gameTitle) {
		this.panelsAsCards = panelsAsCards;
		this.gameTitle = gameTitle;
		createSearchingPanel();
	}

	public void createSearchingPanel() {
		if (gameTitle != null) {
			setDesign();
		}

		roundFinishedButton.addActionListener(this);
		homeButton.addActionListener(this);
	}

	private void setDesign() {
		setLayout(new BorderLayout());
		JPanel navigationPanel = new JPanel();

		navigationPanel.setLayout(new BorderLayout());
		gamePanel.setLayout(new BoxLayout(gamePanel, BoxLayout.Y_AXIS));

		sqliteSetup.openConnection();
		sqliteOperations.setStatement(sqliteSetup.getStatement());
		game = sqliteOperations.returnGameDataFromDb(gameTitle);
		winnerBox = new JComboBox(sqliteOperations.getAllPlayerNamesFromDb());
		winnerBox.setMaximumSize(new Dimension(200, 50));

		labelsMap = labels.prepareLabels(game);
		gamePanel.add(labelsMap.get("title"));
		gamePanel.add(labelsMap.get("timesPlayed"));
		gamePanel.add(labelsMap.get("timeSpent"));
		gamePanel.add(labelsMap.get("lastWinner"));
		gamePanel.add(Box.createRigidArea(new Dimension(0, 25)));
		gamePanel.add(winnerBox);
		gamePanel.add(roundFinishedButton);
		navigationPanel.add(homeButton, BorderLayout.LINE_END);

		add(gamePanel, BorderLayout.CENTER);
		add(navigationPanel, BorderLayout.PAGE_END);

		startTime = System.nanoTime();

	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == homeButton) {
			goToMainMenu();
		}
		if (e.getSource() == roundFinishedButton) {
			long timePlayed = uiControls.countTimeElapsed(startTime);
			long timePlayedTillNow = Long.valueOf(game.getPlayTime()).longValue();
			String timeSum = Long.toString(timePlayed + timePlayedTillNow);
			PrepareQueryForDb prepareQueryForDb = new PrepareQueryForDb();
			String updatePlayerQuery = prepareQueryForDb.updatePlayer(winnerBox.getSelectedItem().toString());
			String updateGameQuery = prepareQueryForDb.updateGame(gameTitle, winnerBox.getSelectedItem().toString(),
					timeSum);
			// update player with win
			// update game with playCount
			// update game with playTime
			// update game with lastWinner
			SqliteSetup sqliteSetup = new SqliteSetup();
			sqliteSetup.openConnection();
			SqliteOperations sqliteOperations = new SqliteOperations();
			sqliteOperations.setStatement(sqliteSetup.getStatement());
			sqliteOperations.executeQuery(updatePlayerQuery);
			sqliteOperations.executeQuery(updateGameQuery);
			goToMainMenu();
		}
	}

	private void goToMainMenu() {
		MenuPanel menuPanel = new MenuPanel(panelsAsCards);
		removeAll();
		add(menuPanel, BorderLayout.CENTER);
		revalidate();
		panelsAsCards.changePanels(MENU);
	}

	

}
