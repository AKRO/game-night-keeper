package view;

import static utils.Dictionary.HOME_BUTTON;
import static utils.Dictionary.MENU;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.*;

import database.*;
import model.Drinks;

public class DrinksPanel extends JPanel implements ActionListener {
	// lista napojow i w jakiej ilosci
	private static final long serialVersionUID = 7301681947443468208L;
	PanelsAsCards panelsAsCards;
	SqliteSetup sqliteSetup = new SqliteSetup();
	SqliteOperations sqliteOperations = new SqliteOperations();
	private JButton homeButton = new JButton(HOME_BUTTON);
	JPanel navigationPanel = new JPanel();
	JPanel drinkViewPanel = new JPanel();

	public DrinksPanel(PanelsAsCards panelsAsCards) {
		this.panelsAsCards = panelsAsCards;
		createDrinksPanel();
	}

	public void createDrinksPanel() {
		setDesign();
		homeButton.addActionListener(this);
	}

	private void setDesign() {
		setLayout(new BorderLayout());

		sqliteSetup.openConnection();
		sqliteOperations.setStatement(sqliteSetup.getStatement());
		ArrayList<Drinks> drinks = (ArrayList<Drinks>) sqliteOperations.returnAllDrinksFromDb();

		setDrinkView();
		fillDrinkView(drinks);
		setNavigationPanel();

		add(drinkViewPanel);
		add(navigationPanel, BorderLayout.PAGE_END);
	}

	private void fillDrinkView(ArrayList<Drinks> drinkList) {
		for (Drinks drink : drinkList) {
			JPanel drinkRecord = new JPanel();
			JButton subtractionButton = new JButton(drink.getDrink());
			drinkRecord.add(subtractionButton);
			subtractionButton.addActionListener(this);
			JLabel labelDrinkAmount = new JLabel(String.valueOf(drink.getAmount()));
			labelDrinkAmount.setName(drink.getDrink());
			drinkRecord.add(labelDrinkAmount);
			drinkViewPanel.add(drinkRecord);
		}
	}

	private void setDrinkView() {
		drinkViewPanel.setLayout(new BoxLayout(drinkViewPanel, BoxLayout.Y_AXIS));
	}

	private void setNavigationPanel() {
		navigationPanel.setLayout(new BorderLayout());
		navigationPanel.add(homeButton, BorderLayout.LINE_END);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == homeButton) {
			MenuPanel menuPanel = new MenuPanel(panelsAsCards);
			removeAll();
			add(menuPanel, BorderLayout.CENTER);
			revalidate();
			panelsAsCards.changePanels(MENU);
		} else {
			AbstractButton btn = (AbstractButton) e.getSource();
			PrepareQueryForDb prepareQueryForDb = new PrepareQueryForDb();
			String updateDrinkQuery = prepareQueryForDb.updateDrink(btn.getText());
			SqliteSetup sqliteSetup = new SqliteSetup();
			sqliteSetup.openConnection();
			SqliteOperations sqliteOperations = new SqliteOperations();
			sqliteOperations.setStatement(sqliteSetup.getStatement());
			sqliteOperations.executeQuery(updateDrinkQuery);

			String getDrinkAmountQuery = prepareQueryForDb.getDrinkAmount(btn.getText());
			sqliteOperations.executeQuery(getDrinkAmountQuery);
		}
	}
}
