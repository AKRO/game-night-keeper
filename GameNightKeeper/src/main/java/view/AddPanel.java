package view;

import static utils.Dictionary.*;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import database.*;

public class AddPanel extends JPanel implements ActionListener {
	private static final long serialVersionUID = 8253404223389181410L;
	PanelsAsCards panelsAsCards;
	JPanel addGamePanel;
	JPanel addPlayerPanel;
	JPanel addDrinkPanel;
	JPanel addStuffButtonPanel;
	PrepareQueryForDb prepareQueryForDb = new PrepareQueryForDb();
	SqliteOperations sqliteOperations;

	private JTextField gameToAddTextField = new JTextField(GAME_NAME);
	private JTextField playerToAddTextField = new JTextField(PLAYER_NAME);
	private JTextField drinkToAddTextField = new JTextField(DRINK_NAME);
	private JTextField amountOfDrinkToAddTextField = new JTextField(DRINK_AMOUNT);
	private JButton buttonAddGame = new JButton(ADD_BUTTON_GAME);
	private JButton buttonAddPlayer = new JButton(ADD_BUTTON_PLAYER);
	private JButton buttonAddDrink = new JButton(ADD_BUTTON_DRINK);
	private JButton homeButton = new JButton(HOME_BUTTON);

	public AddPanel(PanelsAsCards panelsAsCards) {
		this.panelsAsCards = panelsAsCards;
		createAdditionPanel();
		prepareDbConnection();
	}

	public void createAdditionPanel() {
		setDesign();

		buttonAddGame.addActionListener(this);
		buttonAddPlayer.addActionListener(this);
		buttonAddDrink.addActionListener(this);
		homeButton.addActionListener(this);

	}

	private void setDesign() {
		setLayout(new BorderLayout());
		addGamePanel = new JPanel();
		addPlayerPanel = new JPanel();
		addDrinkPanel = new JPanel();
		addStuffButtonPanel = new JPanel();
		addGamePanel.setLayout(new BoxLayout(addGamePanel, BoxLayout.Y_AXIS));
		addPlayerPanel.setLayout(new BoxLayout(addPlayerPanel, BoxLayout.Y_AXIS));
		addDrinkPanel.setLayout(new BoxLayout(addDrinkPanel, BoxLayout.Y_AXIS));
		addStuffButtonPanel.setLayout(new BorderLayout());

		addGamePanel.add(gameToAddTextField);
		addGamePanel.add(buttonAddGame);
		addPlayerPanel.add(playerToAddTextField);
		addPlayerPanel.add(buttonAddPlayer);
		addDrinkPanel.add(drinkToAddTextField);
		addDrinkPanel.add(amountOfDrinkToAddTextField);
		addDrinkPanel.add(buttonAddDrink);

		JPanel navigationPanel = new JPanel();
		navigationPanel.setLayout(new BorderLayout());
		navigationPanel.add(homeButton, BorderLayout.LINE_END);

		JPanel fillerPanel = new JPanel();
		fillerPanel.setLayout(new BorderLayout());
		fillerPanel.add(addGamePanel, BorderLayout.WEST);
		fillerPanel.add(addPlayerPanel, BorderLayout.CENTER);
		fillerPanel.add(addDrinkPanel, BorderLayout.EAST);

		add(fillerPanel, BorderLayout.PAGE_START);
		add(navigationPanel, BorderLayout.PAGE_END);

	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == homeButton) {
			MenuPanel menuPanel = new MenuPanel(panelsAsCards);
			removeAll();
			add(menuPanel, BorderLayout.CENTER);
			revalidate();
			panelsAsCards.changePanels(MENU);
		}
		if (e.getSource() == buttonAddGame) {
			sqliteOperations.executeQuery(prepareQueryForDb.addIntoGames(gameToAddTextField.getText()));
			gameToAddTextField.setText(null);
		}
		if (e.getSource() == buttonAddPlayer) {
			sqliteOperations.executeQuery(prepareQueryForDb.addIntoPlayers(playerToAddTextField.getText()));
			playerToAddTextField.setText(null);
		}
		if (e.getSource() == buttonAddDrink) {
			sqliteOperations.executeQuery(prepareQueryForDb.addIntoDrinks(drinkToAddTextField.getText(),
					Integer.parseInt(amountOfDrinkToAddTextField.getText())));
			drinkToAddTextField.setText(null);
			amountOfDrinkToAddTextField.setText(null);
		}
	}

	private void prepareDbConnection() {
		SqliteSetup sqliteSetup = new SqliteSetup();
		sqliteSetup.openConnection();
		sqliteOperations = new SqliteOperations();
		sqliteOperations.setStatement(sqliteSetup.getStatement());
	}
}
