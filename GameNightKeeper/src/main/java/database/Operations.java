package database;

public interface Operations {
	boolean insertAction(String query);

	boolean updateAction(String query);

	boolean deleteAction(String query);

	void searchAction(String query);

	boolean switchDatabase(String nameOfDatabase);
}
