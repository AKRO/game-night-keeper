package database;

public class Initialization {
	StringBuilder queryToCreateTable;
	SqliteSetup sqliteSetup;

	public void setSqliteSetup(SqliteSetup sqliteSetup) {
		this.sqliteSetup = sqliteSetup;
	}

	public void createGamesTable() {
		queryToCreateTable = new StringBuilder();
		queryToCreateTable.append("CREATE TABLE ").append("Games").append(" (title varchar(255) PRIMARY KEY,")
				.append(" playCount int,").append(" playTime varchar(255),").append(" lastWinner varchar(255)")
				.append(")");

		sqliteSetup.createTable(queryToCreateTable.toString());
	}

	public void createPlayersTable() {
		queryToCreateTable = new StringBuilder();
		queryToCreateTable.append("CREATE TABLE ").append("Players").append(" (name varchar(255) PRIMARY KEY,")
				.append(" wins int").append(")");

		sqliteSetup.createTable(queryToCreateTable.toString());
	}

	public void createDrinksTable() {
		queryToCreateTable = new StringBuilder();
		queryToCreateTable.append("CREATE TABLE ").append("Drinks").append(" (drink varchar(255) PRIMARY KEY,")
				.append(" amount int").append(")");

		sqliteSetup.createTable(queryToCreateTable.toString());
	}

	public void createSnacksTable() {
		queryToCreateTable = new StringBuilder();
		queryToCreateTable.append("CREATE TABLE ").append("Snacks").append(" (snack varchar(255) PRIMARY KEY,")
				.append(" amount int").append(")");

		sqliteSetup.createTable(queryToCreateTable.toString());
	}
}
