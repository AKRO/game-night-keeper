package database;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import model.Drinks;
import model.Game;
import utils.Dictionary;

public class SqliteOperations implements Operations {
	private Statement stat;
	private PrepareQueryForDb actionsWithDb = new PrepareQueryForDb();

	public boolean insertAction(String query) {
		// StringBuilder sbInsert = new StringBuilder("INSERT INTO ");
		// sbInsert = appendToStringBuilder(sbInsert, nameOfTable, givenName,
		// givenSurname, givenBirthDate);
		try {
			stat.execute(query);
		} catch (SQLException e) {
			System.err.println(Dictionary.ERROR_INSERTING_ELEMENT);
			e.printStackTrace();
			System.out.println("Insertion action failed.");

			StringBuilder sbReplace = new StringBuilder("REPLACE INTO ");
			// sbReaplce = appendToStringBuilder(sbReaplce, nameOfTable, givenName,
			// givenSurname, givenBirthDate);
			try {
				stat.execute(sbReplace.toString());
				System.out.println("Replace action successful.");
			} catch (SQLException ex) {
				System.err.println(Dictionary.ERROR_REPLACING_ELEMENT);
				ex.printStackTrace();
				return false;
			}

			return true;
		}

		return true;
	}

	public boolean updateAction(String query) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean deleteAction(String query) {
		// TODO Auto-generated method stub
		return false;
	}

	public void searchAction(String query) {
		// TODO Auto-generated method stub

	}

	private StringBuilder appendToStringBuilder(StringBuilder sb, String nameOfTable, String givenName,
			String givenSurname, String givenBirthDate) {
		sb.append(nameOfTable).append(" (name, surname, birthdate) VALUES ('").append(givenName).append("','")
				.append(givenSurname).append("','").append(givenBirthDate).append("')");

		return sb;
	}

	public void setStatement(Statement stat) {
		this.stat = stat;
	}

	public boolean switchDatabase(String nameOfDatabase) {
		// TODO Auto-generated method stub
		return false;
	}

	public Vector getAllGameTitlesFromDb() {
		Vector<String> vector = new Vector<String>();
		try {
			ResultSet rs = stat.executeQuery(actionsWithDb.returnTitlesFromTableGames());
			while (rs.next()) {
				vector.add(rs.getString("title"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return vector;
	}

	public Vector getAllPlayerNamesFromDb() {
		Vector<String> vector = new Vector<String>();
		try {
			ResultSet rs = stat.executeQuery(actionsWithDb.returnNamesFromTablePlayers());
			while (rs.next()) {
				vector.add(rs.getString("name"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return vector;
	}

	public Game returnGameDataFromDb(String gameTitle) {
		Game game = new Game();
		try {
			ResultSet rs = stat.executeQuery(actionsWithDb.returnAllDataOfGame(gameTitle));
			while (rs.next()) {
				game.setTitle(rs.getString("title"));
				game.setPlayTime(rs.getString("playTime"));
				game.setPlayCount(rs.getInt("playCount"));
				game.setLastWinner(rs.getString("lastWinner"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return game;
	}
	
	public List returnAllDrinksFromDb() {
		ArrayList<Drinks> drinkList = new ArrayList<Drinks>();
		try {
			ResultSet rs = stat.executeQuery(actionsWithDb.returnAllFromTable("drinks"));
			while (rs.next()) {
				Drinks drink = new Drinks();
				drink.setDrink(rs.getString("drink"));
				drink.setAmount(rs.getInt("amount"));
				drinkList.add(drink);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return drinkList;
	}

	public void updateGameStatsAfterFinishedRound(String winner, long timeSpent) {

	}

	public void updatePlayerStatsAfterFinishedRound(String winner) {

	}

	public void executeQuery(String query) {
		try {
			stat.execute(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
