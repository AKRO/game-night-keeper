package database;

import utils.QueryParts;

public class SqliteCreator {
	private Boolean openConnection;
	private Boolean createTable;
	private Boolean closeConnection;
	SqliteSetup sqliteSetup = new SqliteSetup();
	SqliteOperations sqliteOperations = new SqliteOperations();
	Initialization initialization = new Initialization();

	public void prepareDbTables() {
		openConnection = sqliteSetup.openConnection();
		initialization.setSqliteSetup(sqliteSetup);
		initialization.createGamesTable();
		initialization.createPlayersTable();
		initialization.createDrinksTable();
		initialization.createSnacksTable();
		closeConnection = sqliteSetup.closeConnection();
	}

	public void insertElementToTable(String nameOfTable, String title, int playCount, String playTime,
			String lastWinner) {
		sqliteSetup.openConnection();
		sqliteOperations.setStatement(sqliteSetup.getStatement());
		String query = QueryParts.INSERT_INTO + QueryParts.SPACE + nameOfTable + QueryParts.SPACE + QueryParts.VALUES
				+ QueryParts.SPACE + QueryParts.PARENTHESIS_OPEN + QueryParts.QUOTE_SINGLE + title
				+ QueryParts.QUOTE_SINGLE + QueryParts.COMMA + QueryParts.SPACE + playCount + QueryParts.COMMA
				+ QueryParts.SPACE + QueryParts.QUOTE_SINGLE + playTime + QueryParts.QUOTE_SINGLE + QueryParts.COMMA
				+ QueryParts.SPACE + QueryParts.QUOTE_SINGLE + lastWinner + QueryParts.QUOTE_SINGLE
				+ QueryParts.PARENTHESIS_CLOSE + QueryParts.SEMICOLON;
		sqliteOperations.insertAction(query);
		sqliteSetup.closeConnection();
	}

	public Boolean getOpenConnection() {
		return openConnection;
	}

	public Boolean getCreateTable() {
		return createTable;
	}

	public Boolean getCloseConnection() {
		return closeConnection;
	}

}
