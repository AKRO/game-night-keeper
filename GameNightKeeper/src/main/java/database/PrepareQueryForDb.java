package database;

public class PrepareQueryForDb {
	public String returnAllFromTable(String table) {
		return "SELECT * FROM " + table + ";";
	}

	public String returnAllDataOfGame(String gameTitle) {
		return "SELECT * FROM games WHERE title='" + gameTitle + "';";
	}

	public String returnTitlesFromTableGames() {
		return "SELECT title FROM games;";
	}

	public String returnNamesFromTablePlayers() {
		return "SELECT name FROM players;";
	}

	public String addIntoGames(String title) {
		return "INSERT INTO Games (title, playCount, playTime) VALUES ('" + title + "', 0, 0);";
	}

	public String addIntoPlayers(String name) {
		return "INSERT INTO Players (name) VALUES ('" + name + "');";
	}

	public String addIntoDrinks(String drink, int amount) {
		return "INSERT INTO Drinks (drink, amount) VALUES ('" + drink + "', '" + amount + "');";
	}

	public String addIntoSnacks(String snack, int amount) {
		return "INSERT INTO Snacks (snack, amount) VALUES ('" + snack + "', '" + amount + "');";
	}

	public String updatePlayer(String player) {
		return "UPDATE players SET wins = wins + 1 WHERE name = '" + player + "';";
	}

	public String updateGame(String game, String winner, String playTime) {
		return "UPDATE games SET playCount=playCount+1, lastWinner='" + winner + "', playTime='" + playTime
				+ "' WHERE title='" + game + "';";
	}

	public String updateDrink(String drink) {
		return "UPDATE drinks SET amount = amount - 1 WHERE drink = '" + drink + "';";
	}

	public String getDrinkAmount(String drink) {
		return "SELECT amount FROM drinks WHERE drink='" + drink + "';";
	}
}
