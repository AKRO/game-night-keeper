package database;

import java.sql.*;

import utils.Dictionary;

public class SqliteSetup implements Setup {
	private String dbDriver = Dictionary.SQLITE_DRIVER;
	private String dbPath = Dictionary.SQLITE_DB_FILE_PATH;
	private Connection conn;
	private Statement stat;

	public boolean openConnection() {
		try {
			Class.forName(dbDriver);
		} catch (ClassNotFoundException e) {
			System.err.println(Dictionary.MISSING_JDBC_DRIVER);
			e.printStackTrace();
			return false;
		}
		try {
			conn = DriverManager.getConnection(dbPath);
			stat = conn.createStatement();
		} catch (SQLException e) {
			System.err.println(Dictionary.CONNECTION_WITH_DB_FAILURE);
			e.printStackTrace();
			return false;
		}

		return true;
	}

	public boolean closeConnection() {
		try {
			conn.close();
		} catch (SQLException e) {
			System.err.println(Dictionary.ERROR_CLOSING_CONNECTION);
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public boolean createDatabase(String db) {
		// the table creation creates also a db
		return true;
	}

	public boolean createTable(String queryToCreateTable) {
		try {
			stat.execute(queryToCreateTable);
			// setUniqueKey(nameOfTable, "title");
		} catch (SQLException e) {
			System.err.println(Dictionary.ERROR_CREATING_A_TABLE);
			e.printStackTrace();
			System.out.println("Table already exists. Creation has been skipped.");
			return true;
		}
		return true;
	}

	public boolean setUniqueKey(String tableName, String tableField) {

		StringBuilder queryToCreateTable = new StringBuilder();
		queryToCreateTable.append("CREATE UNIQUE INDEX unique_value ON ").append(tableName).append(" (")
				.append(tableField).append(")");
		try {
			stat.execute(queryToCreateTable.toString());
		} catch (SQLException e) {
			System.err.println(Dictionary.ERROR_CREATING_UNIQUE_ID);
			e.printStackTrace();
			return false;
		}
		return true;

	}

	public Statement getStatement() {
		return stat;
	}

	public boolean switchDatabase(String nameOfDb) {
		// TODO Auto-generated method stub
		return false;
	}

}
