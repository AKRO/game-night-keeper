package core;

import java.awt.EventQueue;
import java.io.File;

import database.SqliteCreator;
import view.MainFrame;

public class DbCreator {

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					File database = new File("C:\\CUSTOM\\GameNightKeeper\\DATABASE\\game_night_keper.db");
					if (!database.exists()) {
						SqliteCreator sqliteCreator = new SqliteCreator();
						sqliteCreator.prepareDbTables();
					}
					createGUI();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public static void createGUI() {
		MainFrame mainFrame = new MainFrame();
		mainFrame.prepareFrame();
	}

}