package utils;

public class QueryParts {
	public static final String CREATE = "create";
	public static final String DATABASE = "database";
	public static final String TABLE = "table";
	public static final String INSERT_INTO = "insert into";
	public static final String IF_NOT_EXISTS = "if not exists";
	public static final String IF_EXISTS = "if exists";
	public static final String USE = "use";
	public static final String DROP = "drop";
	public static final String DELETE = "delete";
	public static final String SELECT = "select";
	public static final String FROM = "from";
	public static final String VALUES = "values";
	public static final String PRIMARY_KEY = "primary key";
	public static final String UPDATE = "update";
	public static final String SET = "set";
	public static final String WHERE = "where";
	public static final String AND = "and";
	public static final String RENAME = "rename";
	public static final String TO = "to";

	public static final String MORE = ">";
	public static final String LESS = "<";
	public static final String PARENTHESIS_OPEN = "(";
	public static final String PARENTHESIS_CLOSE = ")";
	public static final String QUOTE_SINGLE = "'";
	public static final String SPACE = " ";
	public static final String COMMA = ",";
	public static final String SEMICOLON = ";";
	public static final String ASTERISK = "*";
	public static final String EQUALLS = "=";
	public static final String PLUS = "+";

	public static final String INT = "int";
	public static final String FLOAT = "float";
	public static final String VARCHAR_50 = "varchar(50)";
	public static final String VARCHAR_255 = "varchar(255)";

}
