package utils;

public class TimeManipulator {
	
	public static int[] splitToComponentTimes(long overallTime) {
		int hours = (int) overallTime / 3600;
		int remainder = (int) overallTime - hours * 3600;
		int mins = remainder / 60;
		remainder = remainder - mins * 60;
		int secs = remainder;

		int[] ints = { hours, mins, secs };
		return ints;
	}

}
