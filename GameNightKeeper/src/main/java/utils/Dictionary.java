package utils;

public class Dictionary {
	public static final String SQLITE_DRIVER = "org.sqlite.JDBC";
	// public static final String MYSQL_DRIVER = "com.mysql.cj.jdbc.Driver";

	// this path saves the db in a specific folder
	public static final String SQLITE_DB_FILE_PATH = "jdbc:sqlite:C:/CUSTOM/GameNightKeeper/DATABASE/game_night_keeper.db";
	// this path saves the db in the project folder
	public static final String SQLITE_DB_FILE_PATH_DEFAULT = "jdbc:sqlite:game_night_keeper.db";
	// public static final String MYSQL_DB_FILE_PATH_LOCALHOST =
	// "jdbc:mysql://localhost:3306?allowPublicKeyRetrieval=true&useSSL=false&serverTimezone=UTC";
	// public static final String MYSQL_DB_USER = "root";
	// public static final String MYSQL_DB_PASSWORD = "MySQL2020";

	public static final String CONNECTION_WITH_DB_FAILURE = "Connection with db failure";
	public static final String ERROR_CLOSING_CONNECTION = "Error: during connection closure";
	public static final String ERROR_CAN_NOT_EXECUTE_STATEMENT = "Error: can not execute statement";
	public static final String ERROR_CREATING_A_TABLE = "Error: creating a table";
	public static final String MISSING_JDBC_DRIVER = "Missing JDBC driver";
	public static final String ERROR_INSERTING_ELEMENT = "Error: cannot insert element";
	public static final String ERROR_REPLACING_ELEMENT = "Error: cannot replace element";
	public static final String ERROR_CREATING_UNIQUE_ID = "Error: could not create unique ID";

	public static final String SQL_STATMNT_IS = "The SQL statement is: ";
	public static final String TOTAL_NUMBER_OF_RECORDS = "Total number of records = ";

	public static final String UNIQUE_VAL_FOR_DUPL = "_2";

	// ** GUI **
	// MainFrame
	public static final String TITLE = "Game night keeper";
	// PanelsAsCards
	public final static String MENU = "Menu";

	public static final String HOME_BUTTON = "HOME";
	public static final String END_GAME_BUTTON = "Finish";
	// AdditionPanel
	public static final String GAME_NAME = "Game?";
	public static final String PLAYER_NAME = "Player?";
	public static final String DRINK_NAME = "Drink?";
	public static final String DRINK_AMOUNT = "Amount?";
	public static final String ADD_BUTTON_GAME = "Add Game";
	public static final String ADD_BUTTON_PLAYER = "Add Player";
	public static final String ADD_BUTTON_DRINK = "Add Drink";

	// MenuPanel
	public static final String PLAY_PANEL_BUTTON = "Play!";
	public static final String DRINKS_PANEL_BUTTON = "Choose a drink.";
	public static final String ADD_PANEL_BUTTON = "Add stuff";

	public final static String ADDITION = "Addition";
	public final static String DRINKS = "Drinks";
	public final static String PLAYING = "Playing";
	// SearchingPanel
	public static final String SEARCH_ONE_TEXTFIELD = "Search for a specific card...";
	public static final String SEARCH_ONE_BUTTON = "Search only for this card.";
	public static final String SEARCH_ALL_BUTTON = "Search for all cards on the list.";

}
