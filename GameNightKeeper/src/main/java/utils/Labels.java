package utils;

import java.util.HashMap;

import javax.swing.JLabel;

import database.SqliteOperations;
import database.SqliteSetup;
import model.Game;

public class Labels {
	
	TimeManipulator timeManipulator = new TimeManipulator();
	
	public HashMap<String, JLabel> prepareLabels(Game game) {
		HashMap<String, JLabel> labelMap = new HashMap<String, JLabel>();

		if(game.getTitle() != null)
		{
			long timePlayed = Long.valueOf(game.getPlayTime()).longValue();
			int[] timeTable = timeManipulator.splitToComponentTimes(timePlayed);
			labelMap.put("title",new JLabel("Game: " + game.getTitle()));
			labelMap.put("timesPlayed", new JLabel("Play count: " + game.getPlayCount()));
			labelMap.put("timeSpent", new JLabel("Time played: " + timeTable[0] + "h : " + timeTable[1] + "m : " + timeTable[2] + "s"));
			labelMap.put("lastWinner", new JLabel("Last winner: " + game.getLastWinner()));
		}
		else
		{
			labelMap.put("title",new JLabel("Game: -"));
			labelMap.put("timesPlayed", new JLabel("Play count: -"));
			labelMap.put("timeSpent", new JLabel("Time played: -h : -m : -s"));
			labelMap.put("lastWinner", new JLabel("Last winner: -" ));
		}
		
		return labelMap;
	}

}
