# Game night keeper

It would be fun to have all the data displayed on screen when playing board games. This helps to keep track of the statistics of each game as well as to organize the night because we know:
<br>
* what games we have
* how much time it usually takes to finish a game
* how many times and how long in total have we played each
* who won last time
* what drinks and snacks we have left (kind of like a menu and a grocery list)
